<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Enseignant;
use App\Entity\Semestre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CoursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('semestre', EntityType::class, [
                'class' => Semestre::class,
                'choice_label' => 'nom'
            ])

            ->add('enseignant', EntityType::class, [
                'class' => Enseignant::class,
                'choice_label' => 'nom'
            ])
            ->add('description')
            ->add('dateCreat')
            ->add('dateModif')
            ->add('ects')
            ->add('nbHTp')
            ->add('nbHTd')
            ->add('nbHCm')
            ->add('semestre')
            ->add('enseignant')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cours::class,
        ]);
    }
}
