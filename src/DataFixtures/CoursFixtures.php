<?php


namespace App\DataFixtures;


use App\Entity\Cours;
use App\Entity\Enseignant;
use App\Entity\Semestre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Provider\DateTime;

class CoursFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');


        //Créer 3 Semestre fakées
        for ($i = 1; $i <=3; $i++) {
            $semestre = new Semestre();
            $semestre->setFormation($faker->sentence())
                ->setSemestre($faker->randomDigit());

            $manager->persist($semestre);

            //Créer 3 Enseignant fakées
            for ($l=1; $l<=3; $l++) {
                $enseignant = new Enseignant();
                $enseignant->setNom($faker->firstNameMale());

                $manager ->persist($enseignant);

                //pour chaque semestre, et chaque enseignant on veut créer entre 4 & 6 cours
                for ($j = 1; $j <= mt_rand(4, 6); $j++) {
                    $cours = new Cours();


                    //$days = (new \DateTime())->diff($cours->getDateCreat())->days;
                    //$minimum = '-' . $days . ' days'; // -100 days

                    $cours->setNom($faker->sentence())
                        ->setDescription($faker->paragraph())
                        ->setDateCreat($faker->dateTimeBetween('-6 months'))
                        ->setDateModif($faker->dateTimeBetween('-2 months'))
                        ->setEcts($faker->randomDigit())
                        ->setNbHTp($faker->randomDigit())
                        ->setNbHTd($faker->randomDigit())
                        ->setNbHCm($faker->randomDigit())
                        ->setSemestre($semestre)
                        ->setEnseignant($enseignant);

                    $manager->persist($cours);

                }
                $manager->flush();
            }
        }


    }
}